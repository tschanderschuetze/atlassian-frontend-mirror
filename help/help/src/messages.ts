import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  help_loading: {
    id: 'help.loading',
    defaultMessage: 'Loading',
    description: '',
  },
  help_header: {
    id: 'help.header',
    defaultMessage: 'Help',
    description: '',
  },
  help_close: {
    id: 'help.close',
    defaultMessage: 'Close',
    description: '',
  },
  help_navigation_back: {
    id: 'help.navigation.back',
    defaultMessage: 'Back',
    description: '',
  },
  help_search_placeholder: {
    id: 'help.search.placeholder',
    defaultMessage: 'Search help articles',
    description: '',
  },
  help_article_rating_title: {
    id: 'help.article_rating.title',
    defaultMessage: 'Was this helpful?',
    description: '',
  },
  help_article_rating_option_yes: {
    id: 'help.article_rating.option.yes',
    defaultMessage: 'Yes',
    description: '',
  },
  help_article_rating_option_no: {
    id: 'help.article_rating.option.no',
    defaultMessage: 'No',
    description: '',
  },
  help_article_rating_form_title: {
    id: 'help.article_rating.form.title',
    defaultMessage: 'Tell us why you gave this rating',
    description: '',
  },
  help_article_rating_accurate: {
    id: 'help.article_rating.accurate',
    defaultMessage: `It isn't accurate`,
    description: '',
  },
  help_article_rating_clear: {
    id: 'help.article_rating.clear',
    defaultMessage: `It isn't clear`,
    description: '',
  },
  help_article_rating_relevant: {
    id: 'help.article_rating.relevant',
    defaultMessage: `It isn't relevant`,
    description: '',
  },
  help_article_rating_form_submit: {
    id: 'help.article_rating.form.submit',
    defaultMessage: `Submit`,
    description: '',
  },
  help_article_rating_form_Success: {
    id: 'help.article_rating.form.success',
    defaultMessage: `Thanks!`,
    description: '',
  },
  help_article_rating_form_failed: {
    id: 'help.article_rating.form.failed',
    defaultMessage: `We couldn't submit your feedback.`,
    description: '',
  },
  help_article_rating_form_failed_try_again: {
    id: 'help.article_rating.form.failed.try_again',
    defaultMessage: `Try again`,
    description: '',
  },
  help_article_rating_form_cancel: {
    id: 'help.article_rating.form.cancel',
    defaultMessage: `Cancel`,
    description: '',
  },
  help_article_rating_form_contact_me: {
    id: 'help.article_rating.form.contact_me',
    defaultMessage: `Atlassian can contact me about this feedback`,
    description: '',
  },
  help_article_list_item_type_whats_new: {
    id: 'help.article_list_item.type.whats_new',
    defaultMessage: `WHAT'S NEW`,
    description: 'Article type title',
  },
  help_article_list_item_type_help_article: {
    id: 'help.article_list_item.type.help-article',
    defaultMessage: `HELP ARTICLE`,
    description: 'Article type title',
  },
  help_article_list_show_more: {
    id: 'help.article_list.show_more',
    defaultMessage: 'Show {numberOfRelatedArticlesLeft} more articles',
    description: '',
  },
  help_article_list_show_less: {
    id: 'help.article_list.show_less',
    defaultMessage: 'Show less',
    description: '',
  },
  help_related_article_title: {
    id: 'help.related_article.title',
    defaultMessage: 'Related articles',
    description: 'Title of the related articles list',
  },
  help_related_article_endpoint_error_title: {
    id: 'help.related_article.endpoint-error.title',
    defaultMessage: `We're having some trouble`,
    description: '',
  },
  help_related_article_endpoint_error_description: {
    id: 'help.related_article.endpoint-error.description',
    defaultMessage: `It's taking us longer than expected to show this content. It's provably a temporary problem.`,
    description: '',
  },
  help_related_article_endpoint_error_button_label: {
    id: 'help.related_article.endpoint-error.button-label',
    defaultMessage: `Try Again`,
    description: '',
  },
  help_search_results_search_external_site: {
    id: 'help.search_results.search_external_site',
    defaultMessage: `Can't find what you're looking for? Try again with a different term or `,
  },
  help_search_results_no_results: {
    id: 'help.search_results.no_results',
    defaultMessage: `We can't find anything matching your search`,
  },
  help_search_results_no_results_line_two: {
    id: 'help.search_results.no_results_line_two',
    defaultMessage: `Try again with a different term or `,
  },
  help_search_results_external_site_link: {
    id: 'help.search_results.external_site_link',
    defaultMessage: `search all online help articles.`,
  },
  help_article_error_title: {
    id: 'help.article_error.title',
    defaultMessage: `Something's gone wrong`,
    description: '',
  },
  help_article_error_text: {
    id: 'help.article_error.text',
    defaultMessage: `Something prevented the page from loading. We will try to reconect automatically`,
    description: '',
  },
  help_article_error_button_label: {
    id: 'help.article_error.button_label',
    defaultMessage: `Try now`,
    description: '',
  },
  help_search_error: {
    id: 'help.search_error',
    defaultMessage: `We are having some trouble`,
    description: '',
  },
  help_search_error_line_two: {
    id: 'help.search_error_line_two',
    defaultMessage: `It's taking us longer than expected to show this content. It's probably a temporary problem.`,
    description: '',
  },
  help_search_error_button_label: {
    id: 'help.search_error.button_label',
    defaultMessage: `Retry`,
    description: '',
  },
});
