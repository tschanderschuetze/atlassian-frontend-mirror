export { chunkinator } from './chunkinator';
export type { Options, Chunk, ChunkinatorFile } from './domain';
