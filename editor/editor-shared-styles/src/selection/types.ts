export enum SelectionStyle {
  Border,
  BoxShadow,
  Background,
  Blanket,
  ResetBorder,
  ResetOpacity,
}
