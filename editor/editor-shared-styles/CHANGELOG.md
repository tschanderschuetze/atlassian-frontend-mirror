# @atlaskit/editor-shared-styles

## 1.1.3

### Patch Changes

- [`a2634b5390`](https://bitbucket.org/atlassian/atlassian-frontend/commits/a2634b5390) - [ux] ED-10780 reduced the threshold for responcive toolbar oayout;fixed problem with italic button not working when it is in collapse menu.

## 1.1.2

### Patch Changes

- [`5f58283e1f`](https://bitbucket.org/atlassian/atlassian-frontend/commits/5f58283e1f) - Export types using Typescript's new "export type" syntax to satisfy Typescript's --isolatedModules compiler option.
  This requires version 3.8 of Typescript, read more about how we handle Typescript versions here: https://atlaskit.atlassian.com/get-started
  Also add `typescript` to `devDependencies` to denote version that the package was built with.

## 1.1.1

### Patch Changes

- Updated dependencies

## 1.1.0

### Minor Changes

- [`fbc358206c`](https://bitbucket.org/atlassian/atlassian-frontend/commits/fbc358206c) - ED-9125 ED-8837 Export values for selected border and selected box shadow, and util to disable browser text selection
- [`4f217f1d92`](https://bitbucket.org/atlassian/atlassian-frontend/commits/4f217f1d92) - ED-10168 Add new package @atlaskit/editor-shared-styles

### Patch Changes

- [`b9812b8b35`](https://bitbucket.org/atlassian/atlassian-frontend/commits/b9812b8b35) - ED-10004 improved editor toolbar responsiveness
