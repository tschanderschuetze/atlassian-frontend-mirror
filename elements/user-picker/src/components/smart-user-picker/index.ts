export { default as SmartUserPicker } from './components';
export type {
  SupportedProduct,
  Props as SmartUserPickerProps,
  State as SmartUserPickerState,
  SmartProps,
} from './components';

export { setEnv as setSmartUserPickerEnv } from './config';
