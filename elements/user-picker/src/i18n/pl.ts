/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Polish
export default {
  'fabric.elements.user-picker.email.add': 'Dodaj użytkownika',
  'fabric.elements.user-picker.email.add.potential':
    'Wprowadź adres e-mail, aby zaprosić',
  'fabric.elements.user-picker.email.select.to.add': 'Wybierz do zaproszenia',
  'fabric.elements.user-picker.group.byline':
    'Grupa zarządzana przez administratora',
  'fabric.elements.user-picker.multi.remove-item': 'Usuń',
  'fabric.elements.user-picker.placeholder':
    'Wprowadź użytkowników lub zespoły…',
  'fabric.elements.user-picker.placeholder.add-more':
    'dodaj więcej użytkowników...',
  'fabric.elements.user-picker.single.clear': 'Wyczyść',
  'fabric.elements.user-picker.team.member.50plus': 'Ponad 50 członków',
  'fabric.elements.user-picker.team.member.50plus.including.you':
    'Ponad 50 członków, łącznie z Tobą',
  'fabric.elements.user-picker.team.member.count':
    '{count, plural, one {{count} członek} few {{count} członków} many {{count} członków} other {{count} członka}}',
  'fabric.elements.user-picker.team.member.count.including.you':
    '{count, plural, one {{count} członek razem z Tobą} few {{count} członków razem z Tobą} many {{count} członków razem z Tobą} other {{count} członka razem z Tobą}}',
};
