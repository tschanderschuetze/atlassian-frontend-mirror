import React from 'react';

import Button from '../../src';

export default () => (
  <Button shouldFitContainer appearance="primary">
    Full width button
  </Button>
);
