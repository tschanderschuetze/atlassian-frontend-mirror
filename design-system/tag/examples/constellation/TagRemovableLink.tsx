import React from 'react';

import Tag from '../../src/tag/removable-tag';

export default () => (
  <Tag
    text="Removable tag link"
    removeButtonLabel="Remove"
    href="/components/tag"
  />
);
