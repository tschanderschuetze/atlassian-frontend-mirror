import React from 'react';

import Tag from '../../src/tag/simple-tag';

const cupcakeipsum = 'Croissant tiramisu gummi bears.';

export default () => <Tag text={cupcakeipsum} />;
