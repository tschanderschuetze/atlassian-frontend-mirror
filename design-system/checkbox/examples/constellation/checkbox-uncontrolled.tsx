import React from 'react';

import { Checkbox } from '../../src';

export default () => (
  <Checkbox
    defaultChecked
    label="Uncontrolled checkbox"
    value="Uncontrolled checkbox"
    name="uncontrolled-checkbox"
  />
);
