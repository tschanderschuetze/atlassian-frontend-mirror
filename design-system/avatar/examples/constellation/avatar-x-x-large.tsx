import React from 'react';

import Avatar from '../../src';

export default function AvatarXXLargeExample() {
  return (
    <div>
      <Avatar size="xxlarge" />
      <Avatar size="xxlarge" appearance="square" />
    </div>
  );
}
